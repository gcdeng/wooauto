// ==UserScript==
// @name         Wooauto
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  automation for wootalk
// @author       You
// @match        https://wootalk.today/
// @grant        none
// ==/UserScript==

// 你的第一句話
const WA_FIRST_SENTENCE = '男';
const WA_TALK_TO_MALE = false;

(function() {
    'use strict';
    let wa_config = {
        detectedSex: {
            female: false,
            male: false
        },
        timer: 3, // 3 secs
        counter: 0,
    };
    const wa_resetConfig = () => {
        wa_config.detectedSex.female = false;
        wa_config.detectedSex.male =  false;
        wa_config.counter = 0;
    };
    const wa_startTalk = () => {
        clickStartChat();
        $('#messageInput').val(WA_FIRST_SENTENCE);
        wa_resetConfig();
    };
    const wa_changePerson = () => {
        changePerson();
        setTimeout(()=>{
            $('#popup-yes').click();
        }, 1500);
    };
    const wa_detect = () => {
        if($('.stranger.text').length>0){
            $('.stranger.text').each((i, stranger)=>{
                if(stranger.innerText.includes('女')){
                    wa_config.detectedSex.female = true;
                    alert("Female detected!");
                } else if (stranger.innerText.includes('男')){
                    wa_config.detectedSex.male = true;
                    if(!WA_TALK_TO_MALE){
                        wa_changePerson();
                    }
                }
            });
        }
    };
    wa_startTalk();
    var wa_checkLeaveId = setInterval(()=>{
        let wa_systemText = $('.system.text');
        if(wa_systemText[wa_systemText.length-2]!=null
        && wa_systemText[wa_systemText.length-2].innerText.indexOf('對方離開了')!=-1){
            changePerson();
            wa_startTalk();
        } else if(dispatcher==null){
            wa_startTalk();
        } else if($('#messageInput').val()===WA_FIRST_SENTENCE){
            sendMessage();
        } else if(!wa_config.detectedSex.female && !wa_config.detectedSex.male){
            wa_detect();
        }
        if(wa_config.counter>30
        && $('.stranger.text').length===0){
            wa_changePerson();
        }
        wa_config.counter+=wa_config.timer;
    }, wa_config.timer*1000);
})();