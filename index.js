var Nightmare = require('nightmare');
var nightmare = Nightmare({ show: true });

nightmare
  .goto('https://wootalk.today/')
  .wait(3000)
  .click('#startButton')
  .wait(3000)
  .insert('#messageInput', '嗨! 男生 台北人')
  .wait(function(){
    x = document.getElementsByClassName('system text');
    if (x[2].innerText == "系統訊息：加密連線完成，開始聊天囉！") {
      return true;
    }
  })
  .click('#sendButton input[type="button"]')
  .wait(function(){
    x = document.getElementsByClassName('system text');
    if (x[x.length-2].innerText == "系統訊息：對方離開了，請按離開按鈕回到首頁") {
      return true;
    }
  })
  .wait(500)
  .click('#changeButton input[type="button"]')
  //.end()
  .then(function(title){
    console.log(title);
  });
